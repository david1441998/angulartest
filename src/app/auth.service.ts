import { User } from './interfaces/user';

import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

import { Observable, Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User | null>;
  private logInSubject = new Subject<string>();
 private signUpErrorSubject = new Subject<string>();
  constructor(public afAuth:AngularFireAuth,
    private router:Router) {
    this.user = this.afAuth.authState;
  }
  

  signup(email:string, password:string){
    this.afAuth
        .auth
        .createUserWithEmailAndPassword(email,password)
        .then(
          res =>{
          console.log('Succesful sign up',res)
          this.router.navigate(['/Logwell'])  .catch(we => this.logInSubject.next(email));;
  }
        )

  }
  logout(){
    this.afAuth.auth.signOut().then(res => console.log('Yahuuu', res));
  }


 login(email:string, password:string){
  this.afAuth
  .auth.signInWithEmailAndPassword(email,password)
  .then(
     res =>  
      {
        console.log('Succesful Login',res);
       
        this.router.navigate(['/Logwell']) .catch(we => this.logInSubject.next(email));;
      }
  )


    }
    public getLogin(): Subject<string> {
      return this.logInSubject;
    }
  }
