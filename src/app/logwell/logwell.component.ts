import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logwell',
  templateUrl: './logwell.component.html',
  styleUrls: ['./logwell.component.css']
})
export class LogwellComponent implements OnInit {
  public email:string;
  constructor(public authService:AuthService,
    private router:Router) { this.authService.getLogin().subscribe(we=>{
                    this.email=we;
                  });}

  ngOnInit() {
    
  }

}
